﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestTask.API.Services.Abstract;
using TestTask.API.Models;

namespace TestTask.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Person")]
    public class PeopleController : Controller
    {
        private readonly IPeopleService peopleService;
        public PeopleController(IPeopleService _service)
        {
            peopleService = _service;
        }

        [Route("list"), HttpGet]
        public List<PeopleModel> GetList()
        {
            return peopleService.GetPeoples();
        }

        [Route("instructions"), HttpGet]
        public string GetInstruction()
        {
            return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id ex magna. Sed massa dolor, elementum et semper in, mattis dapibus orci. In vitae augue non dolor congue accumsan vitae non nisl. Cras gravida metus nec ligula convallis imperdiet. Vestibulum ipsum ante, aliquam nec est vel, rutrum tincidunt sem. In auctor dui et imperdiet pulvinar. Vivamus eu magna at elit dictum posuere.Praesent a lorem egestas orci luctus ornare in id dolor. Vivamus porta, magna non venenatis ultrices, arcu magna consequat felis, id consectetur dolor enim sed ligula.Nulla gravida nibh arcu, in fringilla augue malesuada ac. Etiam at luctus lectus. Sed finibus lectus nec urna pharetra, ac eleifend eros laoreet.Cras mattis suscipit cursus. Integer ac justo a justo efficitur dictum.Etiam accumsan ac lectus eget porttitor. Nam fermentum eget velit eget semper. Praesent et lacinia elit, eget blandit neque. Pellentesque mattis tempor tortor ac congue.";
        }
    }
}