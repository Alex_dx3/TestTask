﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.API.Models;

namespace TestTask.API.Data.Context
{
    public class TestTaskContext : DbContext
    {
        public TestTaskContext(DbContextOptions<TestTaskContext> options)
            : base(options)
        { }

        public DbSet<PeopleModel> Peoples { get; set; }
    }
}
