﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.API.Models;

namespace TestTask.API.Services.Abstract
{
    public interface IPeopleService
    {
        List<PeopleModel> GetPeoples();
    }
}
