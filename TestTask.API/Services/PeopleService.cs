﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.API.Data.Context;
using TestTask.API.Models;
using TestTask.API.Services.Abstract;

namespace TestTask.API.Services
{
    public class PeopleService : IPeopleService
    {
        private readonly TestTaskContext db;
        public PeopleService(TestTaskContext context)
        {
            db = context;
        }
        public List<PeopleModel> GetPeoples()
        {
            return db.Peoples.ToList();
        }
    }
}
