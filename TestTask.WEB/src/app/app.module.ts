import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { MenuComponent } from './menu/menu.component';
import { UserService } from './user/user.service';
import { InstructionComponent } from './instruction/instruction.component';
import { InstructionService } from './instruction/instruction.service';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    MenuComponent,
    InstructionComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    UserService,
    InstructionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
