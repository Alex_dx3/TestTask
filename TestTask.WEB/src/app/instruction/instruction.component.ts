import { Component, OnInit } from '@angular/core';
import { InstructionService } from './instruction.service';

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.css']
})
export class InstructionComponent implements OnInit {
  instruction: string;
  constructor(instructionService: InstructionService) {
    instructionService.GetInstruction().subscribe(result => {
      console.log(result);
      this.instruction = result as string;
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
