import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable()
export class InstructionService {

  constructor(private http: HttpClient) { }

  GetInstruction() {
    return this.http.get(environment.base_url + "/api/person/instructions");
  }
}
