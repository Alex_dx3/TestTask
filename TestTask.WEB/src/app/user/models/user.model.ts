export interface UserModel {
    Id: number,
    FirstName: string,
    LastName: string,
    BirthDay: Date
}