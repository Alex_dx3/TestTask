import { Component, OnInit } from '@angular/core';
import { UserModel } from './models/user.model';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  people: UserModel[];
  constructor(userService: UserService) {
    userService.GetUsers().subscribe(result => {
      console.log(result);
      this.people = result as UserModel[];
    }, error => console.error(error));
  }

  ngOnInit() {
  }

}
