import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  GetUsers() {
    console.log(1);
    return this.http.get(environment.base_url + "/api/person/list");
  }
}
